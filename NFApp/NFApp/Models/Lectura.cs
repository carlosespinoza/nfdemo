﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NFApp.Models
{
    public class Lectura
    {
        public Lectura(string cadena)
        {
            FechaHora = DateTime.Now;
            string[] datos = cadena.Split(' ');
            Valor = int.Parse(datos[0]);
            Porcentaje = float.Parse(datos[1]);
        }

        public DateTime FechaHora { get; private set; }
        public int Valor { get; private set; }
        public float Porcentaje { get; private set; }

    }
}
