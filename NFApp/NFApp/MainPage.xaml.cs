﻿using Microcharts;
using NFApp.Models;
using OpenNETCF.MQTT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace NFApp
{
    public partial class MainPage : ContentPage
    {
        MQTTClient client;
        List<Microcharts.ChartEntry> lecturas;
        public MainPage()
        {
            InitializeComponent();
            client = new MQTTClient("broker.hivemq.com");
            lecturas = new List<ChartEntry>();
            client.MessageReceived += Client_MessageReceived;
            client.Connect("NFApp");
            client.Subscriptions.Add(new Subscription("NFDemo/App"));
            btnApagar.Clicked += BtnApagar_Clicked;
            btnEncender.Clicked += BtnEncender_Clicked;
            btnEnviar.Clicked += BtnEnviar_Clicked;
        }

        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            if (client.IsConnected)
            {
                client.Publish("NFDemo/Hw", Encoding.UTF8.GetBytes($"M{entGrados.Text}"), QoS.FireAndForget, false);
            }
            else
            {
                DisplayAlert("Error", "No estoy conectado...", "Ok");
            }
        }

        private void BtnEncender_Clicked(object sender, EventArgs e)
        {
            if (client.IsConnected)
            {
                client.Publish("NFDemo/Hw", Encoding.UTF8.GetBytes("L1"), QoS.FireAndForget, false);
            }
            else
            {
                DisplayAlert("Error", "No estoy conectado...", "Ok");
            }
        }

        private void BtnApagar_Clicked(object sender, EventArgs e)
        {
            if (client.IsConnected)
            {
                client.Publish("NFDemo/Hw", Encoding.UTF8.GetBytes("L0"), QoS.FireAndForget, false);
            }
            else
            {
                DisplayAlert("Error", "No estoy conectado...", "Ok");
            }
        }

        private void Client_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            string[] datos = Encoding.UTF8.GetString(payload).Split(' ');
            lecturas.Add(new ChartEntry(float.Parse(datos[0]))
            {
                Label = DateTime.Now.ToShortTimeString(),
                ValueLabel = Math.Round(decimal.Parse(datos[1].Replace("%", "")), 2).ToString()
            });
            Grafico.Chart = new LineChart()
            {
                Entries = lecturas
            };
        }
    }
}
