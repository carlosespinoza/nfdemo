using System;
using System.Diagnostics;
using System.Threading;
using System.Device.Gpio;
using Windows.Devices.Adc;
using uPLibrary.Networking.M2Mqtt;
using System.Text;
using System.Net.NetworkInformation;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace NFDemo
{

    public class Program
    {
        static GpioController esp32D;
        static GpioPin lVerde, lRojo, luces;
        static GpioPin in1, in2, in3, in4;

        static AdcController esp32A;
        static AdcChannel potenciometro;

        static Timer pulso, leerPotenciometro;

        static SteeperMotor motor;

       
        const string MqttBroker = "broker.hivemq.com";
        static bool conectado = false;

        static MqttClient mqtt;

        public static void Main()
        {
            esp32D = new GpioController();
            esp32A = AdcController.GetDefault();
            potenciometro = esp32A.OpenChannel(6);

            lVerde = esp32D.OpenPin(23, PinMode.Output);
            lRojo = esp32D.OpenPin(22, PinMode.Output);
            luces = esp32D.OpenPin(21, PinMode.Output);
            in1 = esp32D.OpenPin(4, PinMode.Output);
            in2 = esp32D.OpenPin(0, PinMode.Output);
            in3 = esp32D.OpenPin(2, PinMode.Output);
            in4 = esp32D.OpenPin(15, PinMode.Output);
            motor = new SteeperMotor(in1, in2, in3, in4, 5);
            pulso = new Timer(Conectar, null, 1000, 500);

            luces.Write(PinValue.Low);
            Debug.WriteLine("Hola NanoFramework!");

            try
            {
                Debug.WriteLine("Esperando conexi�n");
                WaitIP();
                conectado = true;
                mqtt = new MqttClient(MqttBroker, 1883, false, null, null, MqttSslProtocols.None);
                mqtt.MqttMsgPublishReceived += Mqtt_MqttMsgPublishReceived;
                mqtt.Subscribe(new string[] { "NFDemo/Hw" }, new byte[] { 0 });
                mqtt.Connect("ESP32");
                leerPotenciometro = new Timer(LeerPotenciometro, null, 2000, 5000);
            }
            catch (Exception ex)
            {
                lRojo.Write(PinValue.High);
                Debug.WriteLine(ex.Message);
            }


            Thread.Sleep(Timeout.Infinite);
        }

        static void WaitIP()
        {
            Debug.WriteLine("Esperando IP...");
            while (true)
            {
                NetworkInterface ni = NetworkInterface.GetAllNetworkInterfaces()[0];
                if (ni.IPv4Address != null && ni.IPv4Address.Length > 0)
                {
                    if (ni.IPv4Address[0] != '0')
                    {
                        Debug.WriteLine("IP: " + ni.IPv4Address);
                        break;
                    }
                }
                Thread.Sleep(1000);
            }
        }



        private static void Mqtt_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string mensaje = UTF8Encoding.UTF8.GetString(e.Message, 0, e.Message.Length);
            Debug.WriteLine($"Recibido: {mensaje}");
            switch (mensaje[0])
            {
                case 'L':
                    if (mensaje[1] == '1')
                    {
                        luces.Write(PinValue.High);
                    }
                    else
                    {
                        luces.Write(PinValue.Low);
                    }
                    break;
                case 'M':
                    motor.GirarGrados(int.Parse(mensaje.Substring(1, mensaje.Length - 1)));
                    break;
                default:
                    break;
            }
        }

        private static void LeerPotenciometro(object state)
        {
            try
            {
                Debug.WriteLine($"Potenciometro: {potenciometro.ReadValue()} {potenciometro.ReadRatio() * 100}%");
                mqtt.Publish("NFDemo/App", Encoding.UTF8.GetBytes($"{potenciometro.ReadValue()} {potenciometro.ReadRatio() * 100}"));
            }
            catch (Exception ex) 
            {
                lRojo.Write(PinValue.High);
                Debug.WriteLine(ex.Message);
            }
            
        }


        private static void Conectar(object state)
        {
            if (!conectado)
            {
                lVerde.Toggle();
            }
            else
            {
                lVerde.Write(PinValue.High);
                pulso.Dispose();
            }
        }
    }
}
